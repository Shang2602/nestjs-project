import { Body, Controller, Post, Req } from '@nestjs/common';
import { Request } from 'express';
import { AuthDto } from './dto';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(private authService : AuthService){}

    @Post('singup')
    signup(@Body() dto : AuthDto){
        console.log({
            dto,
        });
        return this.authService.signup();
    };

    @Post('signin')
    signin(@Body('email') email:String, @Body('password') password:String){
        console.log({
            email,
            password,
        });
        return this.authService.signin();
    }
    
}
